import mysql from '../../config/mysql.js'  

export default async (ctx) => {    
    await mysql('rules').where('id', ctx.params.id).update('active', ctx.request.body.active)  
    ctx.body = { success: true }
}