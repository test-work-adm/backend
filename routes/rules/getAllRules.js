import mysql from '../../config/mysql.js' 

export default async (ctx) => {    
    const language = ctx.query.language ? ctx.query.language : null
    const active = ctx.query.active ? ctx.query.active : null 
    const [dateFrom, dateTo] = ctx.query.dates || [] 
    const prop = ctx.query.prop ? ctx.query.prop : 'created' 
    let order = ctx.query.order ? ctx.query.order : 'asc' 
    if (order === 'descending') {
          order = 'desc'
    } 
    const page = ctx.query.page ? ctx.query.page : 1 
    const limit = 5
    const offset = (page - 1) * limit  

    const mainQuery = (count = false) => mysql('rules').modify(qb => {
            if (!count) {
                qb.select('*')
                qb.orderBy(prop, order)                
            } else {
                qb.count('* as count')   
            }
            if ( language ) {
                qb.where('language', language) 
            } 
            if ( active ) { 
                qb.where('active', +active) 
            } 
            if ( dateFrom ) { 
                qb.whereRaw('created >= FROM_UNIXTIME(?)', +dateFrom / 1000 + 10800)  
            } 
            if ( dateTo ) { 
                qb.whereRaw('created <= FROM_UNIXTIME(?)', +dateTo / 1000 + 97200)  
            }  
        } ) 
    
    const rules = await mainQuery().limit(limit).offset(+offset)
    const total = await mainQuery(true).then(r => r[0].count || 0)    
    ctx.body = {
        total,
        data: rules,
        page: +page,
        pageSize: limit,
    }
}
