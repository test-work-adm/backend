import mysql from '../../config/mysql.js' 
import jwt from "jsonwebtoken"
import { secretKey } from  "../../config/index.js" 

export default async (ctx) => {  
    const ruleData = ctx.request.body
    const token = ctx.request.header.authorization.split(' ') 
    ruleData.user_id = jwt.verify(token[1], secretKey).id  
    const arrId = await mysql('rules').insert(ruleData)
    ruleData.id = arrId[0]
    ctx.body = ruleData
}