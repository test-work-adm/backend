import mysql from '../../config/mysql.js'  

export default async (ctx) => {    
    await mysql('rules').where('id', ctx.params.id).delete()  
    ctx.body = { success: true }
}