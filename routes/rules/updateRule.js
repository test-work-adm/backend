import mysql from '../../config/mysql.js'  

export default async (ctx) => {   
    const obj = ctx.request.body
    delete obj.created
    delete obj.user_id  
    await mysql('rules').where('id', ctx.request.body.id).update(obj) 
    const updatedRule = await mysql('rules').select('*').where('id', ctx.request.body.id) 
    ctx.body = (updatedRule)
}
 