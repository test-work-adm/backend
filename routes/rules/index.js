import Router from "koa-router"
import getAllRules from "./getAllRules.js"   
import createRule from "./createRule.js"
import updateRule from "./updateRule.js"
import deleteRule from "./deleteRule.js"
import activeRule from "./activeRule.js"
import auth from "../../middlewares/auth.js"
import right from "../../middlewares/right.js"

const router = new Router()

router.prefix('/rules') 

router.get('/', auth, getAllRules)  

router.post('/add', auth, right(['admin','manager']), createRule)

router.put('/update', auth, right(['admin','manager']), updateRule)

router.delete('/delete/:id', auth, right(['admin']), deleteRule)

router.patch('/active/:id', auth, right(['admin','manager']), activeRule) 

export default router 