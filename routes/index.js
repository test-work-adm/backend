import Router from "koa-router" 
import users from "./users/index.js"
import rules from "./rules/index.js"

const router = new Router({prefix: '/api'})
 

router.use(users.routes())
router.use(users.allowedMethods())

router.use(rules.routes())
router.use(rules.allowedMethods())

export default router 