import mysql from '../../config/mysql.js' 
import bcrypt from "bcrypt" 

export default async (ctx) => {
    const userData = ctx.request.body
    const candidat = await mysql('users').select('*').where('login', userData.login).first() 
    if (candidat) { 
        throw new Error('Пользователь с таким почтовым адресом уже существует')
    }
    userData.password = bcrypt.hashSync(userData.password, 4)
    await mysql('users').insert(userData)
    ctx.body = userData
}