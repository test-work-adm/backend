import Router from "koa-router"
import getAllUsers from "./getAllUsers.js" 
import registration from "./registration.js" 
import login from "./login.js"   
import fetchMe from "./fetchMe.js"
import auth from "../../middlewares/auth.js"

const router = new Router()

router.prefix('/users') 

router.get('/', auth, getAllUsers) 

router.get('/me', auth, fetchMe) 

router.post('/registration', registration)

router.post('/login', login) 

export default router 