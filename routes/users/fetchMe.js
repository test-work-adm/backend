import mysql from '../../config/mysql.js' 

export default async (ctx) => {   
    const infoUser = await mysql('users').select('id', 'login', 'name').where('id', ctx.user).first()
    const infoRole = await mysql('users_rights').select('*').where('user_id', ctx.user) 
    ctx.body = {
        ...infoUser,
        rights: infoRole.reduce((acc, el) => {
            if (!acc[el.right]) {
                acc[el.right] = true
            }
            return acc
        }, {})
    } 
}