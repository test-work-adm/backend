import mysql from '../../config/mysql.js' 
import bcrypt from "bcrypt" 
import jwt from "jsonwebtoken"
import { secretKey } from  "../../config/index.js" 

const generateAcsessToken = (id) => {
    const payload = {id}
    return jwt.sign(payload, secretKey, {expiresIn: '24h'}) 
}

export default async (ctx) => {  
        const resultUser = await mysql('users').select('*').where('login', ctx.request.body.login).first()  
        if (resultUser) { 
            const passwordHash = resultUser.password  
            const resultPassword = bcrypt.compareSync(ctx.request.body.password, passwordHash) 
            if (resultPassword) {
                const token = generateAcsessToken(resultUser.id)
                ctx.body = token
            } else {
                throw new Error('Указан некорректный пароль')
            }
        } else { 
            throw new Error('Пользователя с таким почтовым адресом не существует')
        } 
}