import Koa from "koa"
import routes from "./routes/index.js"
import { koaBody } from "koa-body"
import cors from "@koa/cors"
import './config/mysql.js' 

const app = new Koa()
 
app.use(cors())
app.use(koaBody())
app.use(routes.routes())
app.use(routes.allowedMethods())

app.listen(3000, () => {
    console.log('server start on 3000 port')
})
