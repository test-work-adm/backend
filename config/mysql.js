import knex from 'knex'
import { database } from './index.js'

const mysql = knex(database)

mysql.raw("SELECT 1").then(() => {
    console.log("DB success connected");
    mysql.migrate.latest().then(function(res) {
        console.log('migrations finished', res)
    });
})
.catch((e) => {
    console.log("DB not connected");
    console.error(e);
});

export default mysql