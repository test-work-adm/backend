import jwt from "jsonwebtoken"
import { secretKey } from "../config/index.js"

export default async (ctx, next) => {
    try {   
        const token = ctx.request.header.authorization.split(' ')[1] 
        if (!token) {
            ctx.status = 401
            ctx.body = { message: 'Отсутствует токен' }
            return
        }
        const decodedToken = jwt.verify(token, secretKey)  
        ctx.user = decodedToken.id
    } catch {
        ctx.status = 401
        ctx.body = { message: 'Пользователь не авторизован' }
        return
    }
    await next()
}