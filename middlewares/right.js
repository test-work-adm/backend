import mysql from '../config/mysql.js' 

export default (right) => {
    right = [right].flat()
    return async (ctx, next) => {
        try { 
            const res = await mysql('users_rights')
                .select('*')
                .where('user_id', ctx.user)
                .whereIn('right', right)
                .first() 
            if (!res) {
                throw new Error('Недостаточно прав')
            }
        } catch (e) {
            console.log(e)
            throw new Error('Недостаточно прав')
        }
        await next()
    }
}