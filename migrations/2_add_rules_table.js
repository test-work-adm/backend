export const up = async (knex) => {
    try {
        await knex.raw(`CREATE TABLE \`rules\` (
            \`id\` int(11) unsigned NOT NULL AUTO_INCREMENT,
            \`title\` varchar(256) NOT NULL,
            \`language\` enum('ru','en') NOT NULL,
            \`content\` text NOT NULL,
            \`active\` tinyint(1) NOT NULL,
            \`created\` timestamp NOT NULL DEFAULT current_timestamp(),
            \`user_id\` int(12) NOT NULL,
            PRIMARY KEY (\`id\`)
          ) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;`)
        await knex('rules').insert({
            title: 'Правило об образе жизни',  
            language: 'ru',
            content: 'Нельзя скипать кодинг, занятия спортом и саморазвитие. Нужно избавиться от вредных привычек, поставить цель, выстроить четкий план и идти к исполнению',
            active: 1,
            created: '2022-12-01 02:43:14',
            user_id: 1
        })
        await knex('rules').insert({
            title: 'Lifestyle rule',  
            language: 'en',
            content: 'You can not discount coding, sports, and self-development. You need to get rid of bad habits, set a goal, build a clear plan and go to fulfillment',
            active: 1,
            created: '2022-12-02 02:43:14',
            user_id: 1
        })
        await knex('rules').insert({
            title: 'Правило об образе жизни',  
            language: 'ru',
            content: 'Нельзя скипать кодинг, занятия спортом и саморазвитие. Нужно избавиться от вредных привычек, поставить цель, выстроить четкий план и идти к исполнению',
            active: 0,
            created: '2022-12-03 02:43:14',
            user_id: 1
        })
        await knex('rules').insert({
            title: 'Lifestyle rule',  
            language: 'en',
            content: 'You can not discount coding, sports, and self-development. You need to get rid of bad habits, set a goal, build a clear plan and go to fulfillment',
            active: 0,
            created: '2022-12-04 02:43:14',
            user_id: 1
        })
        await knex('rules').insert({
            title: 'Правило об образе жизни',  
            language: 'ru',
            content: 'Нельзя скипать кодинг, занятия спортом и саморазвитие. Нужно избавиться от вредных привычек, поставить цель, выстроить четкий план и идти к исполнению',
            active: 1,
            created: '2022-12-05 02:43:14',
            user_id: 1
        })
        await knex('rules').insert({
            title: 'Lifestyle rule',  
            language: 'en',
            content: 'You can not discount coding, sports, and self-development. You need to get rid of bad habits, set a goal, build a clear plan and go to fulfillment',
            active: 0,
            created: '2022-12-06 02:43:14',
            user_id: 1
        })
    } catch (error) {
        console.log(error)
    }
}

export const down = async (knex) => {

}