export const up = async (knex) => {
    try {
        await knex.raw(`CREATE TABLE \`users\` (
            \`id\` int(11) unsigned NOT NULL AUTO_INCREMENT,
            \`name\` varchar(128) NOT NULL,
            \`login\` varchar(128) NOT NULL,
            \`password\` varchar(128) NOT NULL,
            \`created\` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
            PRIMARY KEY (\`id\`)
          ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;`)
        await knex('users').insert({
            name: 'admin',  
            login: 'admin@mail.ru',
            password: '$2b$04$EeRaac8YjVz/RUK1clCOP.CqCyxdUF5.g8dTI7OOVRnnoSiGtePoy'
        })
        await knex('users').insert({
            name: 'manager',  
            login: 'manager@mail.ru',
            password: '$2b$04$QqVgX3fC2D92WMDBqoO/3urxRtjm0Oo/uXZ9HYyLDVAhE2NGg3zzy'
        })
        await knex('users').insert({
            name: 'user',  
            login: 'user@mail.ru',
            password: '$2b$04$FtICkh5IAtR1Bqp4hy2LeeOYcTUVnb6.gYqVGt7EkoIFWgRsmnR46'
        })
    } catch (error) {
        console.log(error)
    }
}

export const down = async (knex) => {

}
  