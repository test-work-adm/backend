export const up = async (knex) => {
    try {
        await knex.raw(`CREATE TABLE \`users_rights\` (
            \`id\` int(11) unsigned NOT NULL AUTO_INCREMENT,
            \`user_id\` int(11) unsigned NOT NULL,
            \`right\` varchar(128) NOT NULL,
            PRIMARY KEY (\`id\`)
          ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;`)

        const admin = await knex('users').select('id').where('login', 'admin@mail.ru').first()
        const manager = await knex('users').select('id').where('login', 'manager@mail.ru').first()
        
        await knex('users_rights').insert({
            user_id: admin.id,  
            right: 'admin' 
        })
        await knex('users_rights').insert({
            user_id: manager.id,  
            right: 'manager' 
        }) 
    } catch (error) {
        console.log(error)
    }
}

export const down = async (knex) => {

}
  